# Freewheel Ad Adapter

## Before starting
- This is an Ad adapter integration guide. If you want a player adapter and plugin integration guide you can find it here
- Before starting, please check out our Environments & Limitations section.

## Quick start
Displayed below you will find the steps you must follow to integrate a YOUBORA ads adapter.

### Using cocoapods
#### Install the youbora library and the related adapter

To install our adapter via cocoapods you have to add the following sources to your podfile:

```ruby
source 'https://bitbucket.org/npaw/ios-sdk-podspecs.git'
source 'https://github.com/CocoaPods/Specs.git'
```

Our adapter will be included with the latest Freewhell. You will need to add the following line to your project’s Podfile dependencies:

```ruby
pod 'YouboraFreewheelAdapter'
```

### Import statements
To do this, just write either of the two lines of code featured below to your import statements.
Objective-c:
```objective-c
@import YouboraFreewheelAdapter;
```
Swift:
```swift
import YouboraFreewheelAdapter;
```

### Init monitoring
You can send an /adInit from the adapter itself, in case you need to notifiy the ad start before having the metada ready. This is optional and you probably won't be needing it
```objective-c
[self.youboraPlugin.adsAdapter fireAdInit]
```

### Start monitoring
The adapter will send AdStart “automatically” when it detects an Ad start, anyway there is the
possibility to send ad start manually calling the method below:
```objective-c
[self.youboraPlugin.adsAdapter fireStart]
```

### Stop monitoring
The adapter will send AdStop "automatically" when it detects an Ad ended, anyway there is the
possibility to send ad stop manually calling the method below:
```objective-c
[self.youboraPlugin.adsAdapter fireStop];
```

## Supported environments &amp; limitations
This section lists which scenarios have been certified by the NPAW team, including any existing limitations.
Generally, NPAW provides active support for the OS versions that have an 80% of the usage share, and
passive support for other versions with at least a 5%.
For a list of statistics relating to device and operating system adoption, view these Mix Panel trends or check the Android Developers Dashboards.

#### Supported environments
If the following scenarios do not match with your existing implementation, please get in touch with
the NPAW team. Additionally, when reaching us, it will be very helpful if you could provide a testing
example with the plugin integrated, so we can evaluate its behavior.

#### Player / SDK version

| Freewheel SDK |
|---------------|
| 6.20          |

#### OS Version

| Active support |
| :------------: |
| 11 |
| 10 |

| Passive support |
| ----- |
| 9 |

#### Streaming protocol &amp; DRM
The YOUBORA plugin supports the most common protocols and is DRM-agnostic.
