## [6.7.0] - 2023-02-03
### Changed
- Make adapter to be compatible with all YouboraLib versions greater than 6.5.0.

## [6.5.3] - 2021-02-18
### Changed
- Update Freewheel to version 6.38.0 using cocoapods

## [6.5.2] - 2020-09-14
### Changed
- Update Freewheel to version 6.37.0

## [6.5.1] - 2019-08-07
### Added
- tvOS support

## [6.5.0] - 2019-08-07
### Added
- New ad methods

## [6.0.7] - 2019-03-04
### Add
- CFBundleShortString is now hardcoded to avoid problems

### Improved
- Same versioning as the library pod

## [6.0.3] - 2019-02-13
### Improved
- Ad title now shows creative id

## [6.0.2] - 2018-11-22
### Fix
- Position reported properly
- Duration reported is now Ad, not Slot

## [6.0.2] - 2018-11-22
### Fix
- Position reported properly
- Duration reported is now Ad, not Slot

## [6.0.1] - 2018-04-19
### Fix
 - Fixed playhead not reseting after an Ad

## [6.0.0] - 2018-03-27
### Release
