//
//  YBFreewheelAdapter.m
//  YouboraFreewheelAdapter
//
//  Created by Enrique Alfonso Burillo on 25/01/2018.
//  Copyright © 2018 Enrique Alfonso. All rights reserved.
//

#import "YBFreewheelAdapter.h"
#import <AdManager/AdManager.h>
// Constants
#define MACRO_NAME(f) #f
#define MACRO_VALUE(f) MACRO_NAME(f)

#define PLUGIN_VERSION_DEF MACRO_VALUE(YOUBORAFREEWHEELADAPTER_VERSION)
#define PLUGIN_NAME_DEF "Freewheel"

#if TARGET_OS_TV==1
#define PLUGIN_PLATFORM_DEF "tvOS"
#else
#define PLUGIN_PLATFORM_DEF "iOS"
#endif

#define PLUGIN_NAME @PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF
#define PLUGIN_VERSION @PLUGIN_VERSION_DEF "-" PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF

typedef id<FWContext> FWPlayer;

@interface YBFreewheelAdapter()

@property (nonatomic, strong) id<FWSlot> currentSlot;

@property (nonatomic, strong) NSNumber* lastPlayhead;

//First buffer is always fake, so we skip it
@property (nonatomic, assign) BOOL firstBuffer;

@end


@implementation YBFreewheelAdapter

- (instancetype)initWithPlayer:(id)player {
    self = [super initWithPlayer:player];
    if(self){
        self.player = player;
    }
    
    return self;
}

- (FWPlayer) getPlayer{
    return (FWPlayer) self.player;
}

- (void)registerListeners {
    [super registerListeners];
    self.firstBuffer = YES;
    @try{
        
        NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
        
        [nc addObserver:self selector:@selector(onRequestComplete:) name:FWRequestCompleteNotification object:self.player];
        [nc addObserver:self selector:@selector(onSlotStarted:) name:FWSlotStartedNotification object:self.player];
        [nc addObserver:self selector:@selector(onSlotEnded:) name:FWSlotEndedNotification object:self.player];
        [nc addObserver:self selector:@selector(onContentPauseRequest:) name:FWContentPauseRequestNotification object:self.player];
        [nc addObserver:self selector:@selector(onContentResumeRequest:) name:FWContentResumeRequestNotification object:self.player];
        [nc addObserver:self selector:@selector(onAdEvent:) name:FWAdEventNotification object:self.player];
    } @catch(NSException *exception){
        
    }
}

- (void) unregisterListeners {
    
    @try{
        
        NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
        
        [nc removeObserver:self name:FWRequestCompleteNotification object:self.player];
        [nc removeObserver:self name:FWSlotStartedNotification object:self.player];
        [nc removeObserver:self name:FWSlotEndedNotification object:self.player];
        [nc removeObserver:self name:FWContentPauseRequestNotification object:self.player];
        [nc removeObserver:self name:FWContentResumeRequestNotification object:self.player];
        [nc removeObserver:self name:FWAdEventNotification object:self.player];
        
    } @catch(NSException *exception){
        [YBLog logException:exception];
    }
    
    [super unregisterListeners];
}

#pragma mark Event methods

- (void)onRequestComplete:(NSNotification *)notification {
    NSError *error = (NSError*)[notification.userInfo objectForKey:FWInfoKeyError];
    if (error != nil) {
        YBAdManifestError manifestError = YBAdManifestWrongResponse;
        [self fireAdManifestWithError:manifestError andMessage:error.localizedDescription];
    } else {
        [self fireAdManifest:nil];
    }
    
}

- (void)onSlotStarted:(NSNotification *)notification {
    NSString *slotCustomId = [notification.userInfo objectForKey:FWInfoKeySlotCustomId];
    self.currentSlot = [self.player getSlotByCustomId:slotCustomId];
    [self fireAdBreakStart];
    //[self fireStart];
}

- (void)onContentPauseRequest:(NSNotification *)notification {
    //Beware, this is called whenever the content is paused, regarding if it is caused by ads or not
    //[self fireStart];
}

- (void)onContentResumeRequest:(NSNotification *)notification {
    //[self fireStop];
}

- (void)onAdEvent:(NSNotification *)notification {
    if(self.currentSlot != nil){
        if([self.currentSlot timePositionClass] != FWTimePositionClassPreroll
           && [self.currentSlot timePositionClass] != FWTimePositionClassMidroll
           && [self.currentSlot timePositionClass] != FWTimePositionClassPostroll){
            return;
        }
    }
    
    NSString *eventName = [notification.userInfo objectForKey:FWInfoKeyAdEventName];
    
    if([eventName isEqualToString:FWAdImpressionEvent]){
        [self fireStart];
        [self fireJoin];
    }
    
    if([eventName isEqualToString:FWAdPauseEvent]){
        self.lastPlayhead = [self getPlayhead] ;
        [self firePause];
    }
    
    if([eventName isEqualToString:FWAdResumeEvent]){
        [self fireResume];
    }
    
    if([eventName isEqualToString:FWAdClickEvent]){
        [self fireClick];
    }
    
    if([eventName isEqualToString:FWAdImpressionEndEvent]){
        [self fireStop];
    }
    
    if([eventName isEqualToString:FWAdBufferingStartEvent]){
        [self fireBufferBegin];
    }
    
    if([eventName isEqualToString:FWAdBufferingEndEvent]){
        [self fireBufferEnd];
    }
    
    if([eventName isEqualToString:FWAdErrorEvent]){
        NSString *message = @"Unknown error";
        NSString *code = @"9000";
        if(notification.userInfo != nil){
            message = [notification.userInfo objectForKey:FWInfoKeyErrorInfo];
            code = [notification.userInfo objectForKey:FWInfoKeyErrorCode];
        }
        if(!self.flags.started){
            [self fireStart];
        }
        [self fireErrorWithMessage:message code:code andMetadata:nil];
        [self fireStop];
    }
    
    if ([eventName isEqualToString:FWAdFirstQuartileEvent]) {
        [self fireQuartile:1];
    }
    
    if ([eventName isEqualToString:FWAdMidpointEvent]) {
        [self fireQuartile:2];
    }
    
    if ([eventName isEqualToString:FWAdThirdQuartileEvent]) {
        [self fireQuartile:3];
    }
    
}


- (void)onSlotEnded:(NSNotification *)notification {
    [self fireStop];
    [self fireAdBreakStop];
}

- (void) fireStop{
    NSDictionary *params = [[NSDictionary alloc] init];
    if(self.lastPlayhead != nil){
        params = @{
                   @"adPlayhead" : [[self getDuration] stringValue]
                   };
    }
    [self fireStop:params];
    self.lastPlayhead = nil;
}

#pragma mark Info methods

- (YBAdPosition) getPosition{
    if (self.currentSlot == nil) {
        return YBAdPositionUnknown;
    }
    
    if (self.plugin != nil && self.plugin.adapter != nil){
        if(!self.plugin.adapter.flags.joined){
            return YBAdPositionPre;
        }
        if(!self.plugin.adapter.flags.stopped){
            return YBAdPositionMid;
        }
        if(self.plugin.adapter.flags.stopped){
            return YBAdPositionPost;
        }
        return YBAdPositionUnknown;
    }
    
    //Fallback in case of null adapter / plugin
    if([self.currentSlot timePositionClass] == FWTimePositionClassPreroll){
        return YBAdPositionPre;
    }
    
    if([self.currentSlot timePositionClass] != FWTimePositionClassMidroll){
        return YBAdPositionMid;
    }
    
    if([self.currentSlot timePositionClass] != FWTimePositionClassPostroll){
        return YBAdPositionPost;
    }
    
    return YBAdPositionUnknown;
}

- (NSNumber*) getPlayhead{
    double currentTime = self.currentSlot.currentAdInstance.playheadTime;
    self.lastPlayhead = self.currentSlot == nil ? [super getPlayhead] : @(currentTime);
    return self.lastPlayhead;
}

- (NSNumber*) getDuration{
    return self.currentSlot == nil ? [super getDuration] : @(self.currentSlot.currentAdInstance.duration);
}

- (NSString*) getTitle{
    if(self.currentSlot == nil){
        return [super getTitle];
    }
    return [NSString stringWithFormat:@"creativeId: %tu", self.currentSlot.currentAdInstance.creativeId];
    //return [NSString stringWithFormat:@"Freewheel slotId : %@",self.currentSlot.customId];
}

- (NSNumber *) getAdGivenBreaks {
    return @([self getPlayer].temporalSlots.count);
}

- (NSArray *) getAdBreaksTime {
    NSArray * temporalSlots = [self getPlayer].temporalSlots;
    NSMutableArray * slotsTime = [[NSMutableArray alloc] init];
    for (int k = 0 ; k < [temporalSlots count] ; k++) {
        id<FWSlot> currSlot = temporalSlots[k];
        [slotsTime addObject:@(currSlot.timePosition)];
    }
    return slotsTime;
}

- (NSNumber *) getGivenAds {
    if (self.currentSlot != nil) {
        return @(self.currentSlot.adInstances.count);
    }
    return [super getGivenAds];
}

- (NSString *)getPlayerName {
    return PLUGIN_NAME;
}

- (NSString *)getPlayerVersion {
    return @PLUGIN_NAME_DEF "-" FW_SDK_VERSION;
}

- (NSString *)getVersion {
    return PLUGIN_VERSION;
}

@end
