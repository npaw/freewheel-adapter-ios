//
//  FreeWheelAdManager.m
//  PrerollDemo
//
//  Created by Shitong (Stone) Wang on 3/23/16.
//  Copyright © 2016 Shitong (Stone) Wang. All rights reserved.
//
#import "FreeWheelAdManager.h"

#define kDemoNetworkId 96749

@implementation FreeWheelAdManager

+ (id<FWAdManager>)sharedAdManager {
	static id<FWAdManager> adm;
	static dispatch_once_t t;
	dispatch_once(&t, ^{
		adm = newAdManager();
		// Consult your FreeWheel Solution Engineer about the following values to use in your own app:
		[adm setNetworkId:kDemoNetworkId];
	});
	return adm;
}
@end
