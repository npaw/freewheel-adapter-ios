//
//  AppDelegate.h
//  PrerollDemo
//
//  Created by Shitong (Stone) Wang on 4/21/16.
//  Copyright © 2016 Shitong (Stone) Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

